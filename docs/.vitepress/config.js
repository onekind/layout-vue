import packageJson from '../../package.json'
import { defineConfig } from 'vitepress'

const sidebar = () => {
  return [
    {
      text: 'Introduction',
      children: [
        { text: 'What is Layout Vue?', link: '/' },
        { text: 'Instalation', link: '/instalation' },
      ],
    },
    {
      text: 'Usage',
      children: [
        { text: 'The styles', link: '/guide/styles' },
        { text: 'Direction', link: '/guide/direction' },
        { text: 'Gaps', link: '/guide/gaps' },
        { text: 'Grid', link: '/guide/grid' },
        { text: 'Horizontal alignment', link: '/guide/horizontal-alignment' },
        { text: 'Order', link: '/guide/order' },
        { text: 'Vertical alignment', link: '/guide/vertical-alignment' },
        { text: 'Visibility', link: '/guide/visibility' },
        { text: 'Wrap', link: '/guide/wrap' },
      ],
    },
    {
      text: 'Extras',
      children: [
        { text: 'Guides', link: '/guide/guides' },
      ],
    },
  ]
}

export default defineConfig({
  lang: 'en-GB',
  base: '/layout-vue/',
  title: 'layout-vue',
  description: packageJson.description,
  outDir: '../public',

  themeConfig: {
    repo: 'https://gitlab.com/onekind/layout-vue',
    docsDir: 'docs',
    editLinks: true,
    // logo: '/logo.png',
    /* algolia: {
      appId: '',
      apiKey: '',
      indexName: 'layout-vue'
    }, */
    /* carbonAds: {
      carbon: '',
      custom: '',
      placement: 'onekind'
    }, */
    nav: [
      { text: 'Guide', link: '/', activeMatch: '^/$|^/guide/' },
      {
        text: 'Release Notes',
        link: 'https://gitlab.com/onekind/onekind-vue/-/releases',
      },
      {
        text: 'Source',
        link: 'https://gitlab.com/onekind/layout-vue.git',
      },
    ],
    sidebar: {
      '/guide/': sidebar(),
      '/': sidebar(),
    },
  },
})
