# Gaps

- **Basic** `gap="[extra-small, small, medium, large, extra-large]"`

<div class="sample">
  <ok-layout dir="row" gap="extra-small" >
    <ok-layout dir="col">extra-small</ok-layout>
    <ok-layout dir="col">extra-small</ok-layout>
  </ok-layout>
  <ok-layout dir="row" gap="small" >
    <ok-layout dir="col">small</ok-layout>
    <ok-layout dir="col">small</ok-layout>
  </ok-layout>
  <ok-layout dir="row" gap="medium" >
    <ok-layout dir="col">medium</ok-layout>
    <ok-layout dir="col">medium</ok-layout>
  </ok-layout>
  <ok-layout dir="row" gap="large" >
    <ok-layout dir="col">large</ok-layout>
    <ok-layout dir="col">large</ok-layout>
  </ok-layout>
  <ok-layout dir="row" gap="extra-large" >
    <ok-layout dir="col">extra-large</ok-layout>
    <ok-layout dir="col">extra-large</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1,5,9,13,17}
<ok-layout dir="row" gap="extra-small" >
  <ok-layout dir="col">extra-small</ok-layout>
  <ok-layout dir="col">extra-small</ok-layout>
</ok-layout>
<ok-layout dir="row" gap="small" >
  <ok-layout dir="col">small</ok-layout>
  <ok-layout dir="col">small</ok-layout>
</ok-layout>
<ok-layout dir="row" gap="medium" >
  <ok-layout dir="col">medium</ok-layout>
  <ok-layout dir="col">medium</ok-layout>
</ok-layout>
<ok-layout dir="row" gap="large" >
  <ok-layout dir="col">large</ok-layout>
  <ok-layout dir="col">large</ok-layout>
</ok-layout>
<ok-layout dir="row" gap="extra-large" >
  <ok-layout dir="col">extra-large</ok-layout>
  <ok-layout dir="col">extra-large</ok-layout>
</ok-layout>
```
:::

<div class="sample">
  <ok-layout dir="col" gap="extra-small">
    <ok-layout dir="row">extra-small</ok-layout>
    <ok-layout dir="row">extra-small</ok-layout>
  </ok-layout>
  <ok-layout dir="col" gap="small">
    <ok-layout dir="row">small</ok-layout>
    <ok-layout dir="row">small</ok-layout>
  </ok-layout>
  <ok-layout dir="col" gap="medium">
    <ok-layout dir="row">medium</ok-layout>
    <ok-layout dir="row">medium</ok-layout>
  </ok-layout>
  <ok-layout dir="col" gap="large">
    <ok-layout dir="row">large</ok-layout>
    <ok-layout dir="row">large</ok-layout>
  </ok-layout>
  <ok-layout dir="col" gap="extra-large">
    <ok-layout dir="row">extra-large</ok-layout>
    <ok-layout dir="row">extra-large</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1,5,9,13,17}
<ok-layout dir="col" gap="extra-small">
  <ok-layout dir="row">extra-small</ok-layout>
  <ok-layout dir="row">extra-small</ok-layout>
</ok-layout>
<ok-layout dir="col" gap="small">
  <ok-layout dir="row">small</ok-layout>
  <ok-layout dir="row">small</ok-layout>
</ok-layout>
<ok-layout dir="col" gap="medium">
  <ok-layout dir="row">medium</ok-layout>
  <ok-layout dir="row">medium</ok-layout>
</ok-layout>
<ok-layout dir="col" gap="large">
  <ok-layout dir="row">large</ok-layout>
  <ok-layout dir="row">large</ok-layout>
</ok-layout>
<ok-layout dir="col" gap="extra-large">
  <ok-layout dir="row">extra-large</ok-layout>
  <ok-layout dir="row">extra-large</ok-layout>
</ok-layout>
```
:::

- **Responsive** `:gap="{phoneSmall: 'small', tabletSmall: 'extra-large'}"`

::: info
You need to modify the viewport `width` to see the result.
:::

<div class="sample">
  <ok-layout dir="row" :gap="{phoneSmall: 'small', tabletSmall: 'extra-large'}">
    <ok-layout dir="col" grow>Sample</ok-layout>
    <ok-layout dir="col" grow>Sample</ok-layout>
  </ok-layout>
</div>

<div class="sample">
  <ok-layout dir="col" :gap="{phoneSmall: 'small', tabletSmall: 'extra-large'}">
    <ok-layout dir="row">Sample</ok-layout>
    <ok-layout dir="row">Sample</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1,5}
<ok-layout dir="row" :gap="{phoneSmall: 'small', tabletSmall: 'extra-large'}">
  <ok-layout dir="col" grow>Sample</ok-layout>
  <ok-layout dir="col" grow>Sample</ok-layout>
</ok-layout>
<ok-layout dir="col" :gap="{phoneSmall: 'small', tabletSmall: 'extra-large'}">
  <ok-layout dir="row">Sample</ok-layout>
  <ok-layout dir="row">Sample</ok-layout>
</ok-layout>
```
:::
