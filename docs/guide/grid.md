# Grid

The maximum number of columns is by default defined as `12`

- **Basic** `grid="[number]"`

<div class="sample">
  <ok-layout dir="row">
    <ok-layout dir="col" grid="12">12</ok-layout>
  </ok-layout>
  <ok-layout dir="row">
    <ok-layout dir="col" grid="1">1</ok-layout>
    <ok-layout dir="col" grid="11">11</ok-layout>
  </ok-layout>
  <ok-layout dir="row">
    <ok-layout dir="col" grid="2">2</ok-layout>
    <ok-layout dir="col" grid="10">10</ok-layout>
  </ok-layout>
  <ok-layout dir="row">
    <ok-layout dir="col" grid="3">3</ok-layout>
    <ok-layout dir="col" grid="9">9</ok-layout>
  </ok-layout>
  <ok-layout dir="row">
    <ok-layout dir="col" grid="4">4</ok-layout>
    <ok-layout dir="col" grid="8">8</ok-layout>
  </ok-layout>
  <ok-layout dir="row">
    <ok-layout dir="col" grid="5">5</ok-layout>
    <ok-layout dir="col" grid="7">7</ok-layout>
  </ok-layout>
  <ok-layout dir="row">
    <ok-layout dir="col" grid="6">6</ok-layout>
    <ok-layout dir="col" grid="6">6</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{2,5-6,9-10,13-14,17-18,21-22,25-26}
<ok-layout dir="row">
  <ok-layout dir="col" grid="12">12</ok-layout>
</ok-layout>
<ok-layout dir="row">
  <ok-layout dir="col" grid="1">1</ok-layout>
  <ok-layout dir="col" grid="11">11</ok-layout>
</ok-layout>
<ok-layout dir="row">
  <ok-layout dir="col" grid="2">2</ok-layout>
  <ok-layout dir="col" grid="10">10</ok-layout>
</ok-layout>
<ok-layout dir="row">
  <ok-layout dir="col" grid="3">3</ok-layout>
  <ok-layout dir="col" grid="9">9</ok-layout>
</ok-layout>
<ok-layout dir="row">
  <ok-layout dir="col" grid="4">4</ok-layout>
  <ok-layout dir="col" grid="8">8</ok-layout>
</ok-layout>
<ok-layout dir="row">
  <ok-layout dir="col" grid="5">5</ok-layout>
  <ok-layout dir="col" grid="7">7</ok-layout>
</ok-layout>
<ok-layout dir="row">
  <ok-layout dir="col" grid="6">6</ok-layout>
  <ok-layout dir="col" grid="6">6</ok-layout>
</ok-layout>
```
:::

- **Responsive** `:grid="{phoneSmall: 8, tabletSmall: 4}"` and `:grid="{phoneSmall: 4, tabletSmall: 8}"`

::: info
You need to modify the viewport `width` to see the result.
:::

<div class="sample">
  <ok-layout dir="row">
    <ok-layout dir="col" :grid="{phoneSmall: 8, tabletSmall: 4}">Sample</ok-layout>
    <ok-layout dir="col" :grid="{phoneSmall: 4, tabletSmall: 8}">Sample</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue
<ok-layout :dir="{phoneSmall: 'col', tabletSmall: 'row'}">
  <ok-layout dir="row" :grid="{phoneSmall: 12, tabletSmall: 3}" />
  <ok-layout dir="row" :grid="{phoneSmall: 12, tabletSmall: 9}" />
</ok-layout>
```
:::

- **Shift** `:shift="[number, string, object]"`

<div class="sample">
  <ok-layout dir="col">
    <ok-layout dir="row" justify="end" grid="1" shift="11">11/1</ok-layout>
    <ok-layout dir="row" justify="end" grid="2" shift="10">10/2</ok-layout>
    <ok-layout dir="row" justify="end" grid="3" shift="9">9/3</ok-layout>
    <ok-layout dir="row" justify="end" grid="4" shift="8">8/4</ok-layout>
    <ok-layout dir="row" justify="end" grid="5" shift="7">7/5</ok-layout>
    <ok-layout dir="row" justify="end" grid="6" shift="6">6/6</ok-layout>
    <ok-layout dir="row" justify="end" grid="7" shift="5">5/7</ok-layout>
    <ok-layout dir="row" justify="end" grid="8" shift="4">4/8</ok-layout>
    <ok-layout dir="row" justify="end" grid="9" shift="3">3/9</ok-layout>
    <ok-layout dir="row" justify="end" grid="10" shift="2">2/10</ok-layout>
    <ok-layout dir="row" justify="end" grid="11" shift="1">1/11</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue
<ok-layout dir="col">
  <ok-layout dir="row" justify="end" grid="1" shift="11">11/1</ok-layout>
  <ok-layout dir="row" justify="end" grid="2" shift="10">10/2</ok-layout>
  <ok-layout dir="row" justify="end" grid="3" shift="9">9/3</ok-layout>
  <ok-layout dir="row" justify="end" grid="4" shift="8">8/4</ok-layout>
  <ok-layout dir="row" justify="end" grid="5" shift="7">7/5</ok-layout>
  <ok-layout dir="row" justify="end" grid="6" shift="6">6/6</ok-layout>
  <ok-layout dir="row" justify="end" grid="7" shift="5">5/7</ok-layout>
  <ok-layout dir="row" justify="end" grid="8" shift="4">4/8</ok-layout>
  <ok-layout dir="row" justify="end" grid="9" shift="3">3/9</ok-layout>
  <ok-layout dir="row" justify="end" grid="10" shift="2">2/10</ok-layout>
  <ok-layout dir="row" justify="end" grid="11" shift="1">1/11</ok-layout>
</ok-layout>
```
:::
