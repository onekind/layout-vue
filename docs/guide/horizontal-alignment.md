# Horizontal alignment

- **Basic** `left`, `center`, `right`, `around`, `between` and `evenly`

<div class="sample">
  <ok-layout dir="row" justify="start">
    <ok-layout dir="col">left</ok-layout>
  </ok-layout>
  <ok-layout dir="row" justify="center">
    <ok-layout dir="col">center</ok-layout>
  </ok-layout>
  <ok-layout dir="row" justify="end">
    <ok-layout dir="col">right</ok-layout>
  </ok-layout>
  <ok-layout dir="row" justify="around">
    <ok-layout dir="col">around</ok-layout>
    <ok-layout dir="col">around</ok-layout>
    <ok-layout dir="col">around</ok-layout>
  </ok-layout>
  <ok-layout dir="row" justify="between">
    <ok-layout dir="col">between</ok-layout>
    <ok-layout dir="col">between</ok-layout>
    <ok-layout dir="col">between</ok-layout>
  </ok-layout>
  <ok-layout dir="row" justify="evenly">
    <ok-layout dir="col">evenly</ok-layout>
    <ok-layout dir="col">evenly</ok-layout>
    <ok-layout dir="col">evenly</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1,4,7,10,15,20}
<ok-layout dir="row" justify="start">
  <ok-layout dir="col">left</ok-layout>
</ok-layout>
<ok-layout dir="row" justify="center">
  <ok-layout dir="col">center</ok-layout>
</ok-layout>
<ok-layout dir="row" justify="end">
  <ok-layout dir="col">right</ok-layout>
</ok-layout>
<ok-layout dir="row" justify="around">
  <ok-layout dir="col">around</ok-layout>
  <ok-layout dir="col">around</ok-layout>
  <ok-layout dir="col">around</ok-layout>
</ok-layout>
<ok-layout dir="row" justify="between">
  <ok-layout dir="col">between</ok-layout>
  <ok-layout dir="col">between</ok-layout>
  <ok-layout dir="col">between</ok-layout>
</ok-layout>
<ok-layout dir="row" justify="evenly">
  <ok-layout dir="col">evenly</ok-layout>
  <ok-layout dir="col">evenly</ok-layout>
  <ok-layout dir="col">evenly</ok-layout>
</ok-layout>
```
:::

- **Responsive** `:justify="{phoneSmall: 'center', tabletSmall: 'left'}"`

::: info
You need to modify the viewport `width` to see the result.
:::

<div class="sample">
  <ok-layout dir="row" :justify="{phoneSmall: 'center', tabletSmall: 'left'}">
    <ok-layout dir="col">aligned center or left</ok-layout>
  </ok-layout>
  <ok-layout dir="row" :justify="{phoneSmall: 'center'}">
    <ok-layout dir="col">aligned center always</ok-layout>
  </ok-layout>
  <ok-layout dir="row" :justify="{phoneSmall: 'center', tabletSmall: 'right'}">
    <ok-layout dir="col">aligned center or right</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1,4,7}
<ok-layout dir="row" :justify="{phoneSmall: 'center', tabletSmall: 'left'}">
  <ok-layout dir="col">aligned center or left</ok-layout>
</ok-layout>
<ok-layout dir="row" :justify="{phoneSmall: 'center'}">
  <ok-layout dir="col">aligned center always</ok-layout>
</ok-layout>
<ok-layout dir="row" :justify="{phoneSmall: 'center', tabletSmall: 'right'}">
  <ok-layout dir="col">aligned center or right</ok-layout>
</ok-layout>
```
:::
