# Instalation

```bash
yarn add @onekind/layout-vue
```

- Add the following to you application main.js file:

```js
import components from '@onekind/layout-vue'
import '@onekind/layout-vue/dist/layout-vue.css'
app.use(components)
```

## Customizing

You may use the `scss` files instead, which offer more customazation.

- **breakpoints**: Optionally setup your breakpoint names and values, before you import the main `scss` file.

```scss
$breakpoints: (
  "phone": 320,
  "tablet": 425,
  "desktop": 1024
);
```

- **width**: Optionally setup a new application max layout width, before you import the main `scss` file.

```scss
$layout-max-width: 1080;
```

- **gap**: Optionally setup new gap base value and gap rations, before you import the main `scss` file.

```scss
$layout-gap-base: 16;
```

```scss
@use "sass:math";
$gap-ratios: (
  "small": math.div($layout-gap-base, 2)px,
  "medium": #{$layout-gap-base}px,
  "large": #{$layout-gap-base * 2}px,
);
```

- **columns**: Optionally setup new column limit for the guides helper, before you import the main `scss` file.

```scss
$layout-columns: 1080;
```

- Import the main style `scss` file.

```scss
@import '@onekind/layout-vue/src/styles/layout.scss';
```
