# Guides

<ok-guides visible />

## Purpose

Guides are an excelent way of visually inspecting the layout of your application. They respect the `$breakpoints` and `$gaps` provided to the `layout` component, so you should always see a relationship between your grid choices, and where the guide lines are.

## Setup

::: warning For development only
Guides should not be visible in production.
One solution is to initialize guides based on the `NODE_ENV` environment variable.
So, make sure you use `v-if`
:::

On your page controller, add the following:

```vue
  <ok-guides />
```

## Enabling

Guides are activated with the default key **`"g"`**.
