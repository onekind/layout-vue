# Vertical alignment

- **Basic** `top`, `center` and `bottom`

<div class="sample">
  <ok-layout dir="row" align="start" style="height: 100px">
    <ok-layout dir="col" grow>top</ok-layout>
  </ok-layout>
  <ok-layout dir="row" align="center" style="height: 100px">
    <ok-layout dir="col" grow>center</ok-layout>
  </ok-layout>
  <ok-layout dir="row" align="end" style="height: 100px">
    <ok-layout dir="col" grow>bottom</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1,4,7}
<ok-layout dir="row" align="start" style="height: 100px">
  <ok-layout dir="col" grow>top</ok-layout>
</ok-layout>
<ok-layout dir="row" align="center" style="height: 100px">
  <ok-layout dir="col" grow>center</ok-layout>
</ok-layout>
<ok-layout dir="row" align="end" style="height: 100px">
  <ok-layout dir="col" grow>bottom</ok-layout>
</ok-layout>
```
:::

- **Responsive** `:align="{phoneSmall: 'top', tabletSmall: 'bottom'}"`

::: info
You need to modify the viewport `width` to see the result.
:::

<div class="sample">
  <ok-layout dir="row" :align="{phoneSmall: 'top', tabletSmall: 'bottom'}" style="height: 100px">
    <ok-layout dir="col" grow>Top or Bottom</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1}
<ok-layout dir="row" :align="{phoneSmall: 'top', tabletSmall: 'bottom'}" style="height: 100px">
  <ok-layout dir="col" grow>Top or Bottom</ok-layout>
</ok-layout>
```
:::
