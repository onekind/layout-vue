## [2.1.3](https://gitlab.com/onekind/layout-vue/compare/v2.1.2...v2.1.3) (2023-01-23)


### Bug Fixes

* **ci:** force node 18 ([009b4cf](https://gitlab.com/onekind/layout-vue/commit/009b4cfb5948dfc78e323e093d0a7c27dd4e4a15))
* documentation error in vertical align ([a3dd8d3](https://gitlab.com/onekind/layout-vue/commit/a3dd8d3d8f2300987f03597e4319030f1e3605dc))

## [2.1.2](https://gitlab.com/onekind/layout-vue/compare/v2.1.1...v2.1.2) (2022-06-24)


### Bug Fixes

* update README.md badges ([9cf4da6](https://gitlab.com/onekind/layout-vue/commit/9cf4da66def0c9b04a50374b981a7954bf7de358))

## [2.1.1](https://gitlab.com/onekind/layout-vue/compare/v2.1.0...v2.1.1) (2022-03-19)


### Bug Fixes

* **layout:** missing flex import from props ([680709e](https://gitlab.com/onekind/layout-vue/commit/680709ee340692ead3c3ae57d11eebdbb228e7ab))

# [2.1.0](https://gitlab.com/onekind/layout-vue/compare/v2.0.4...v2.1.0) (2022-03-18)


### Features

* **flex:** introduce flex to allow containers to grow ([0e5a662](https://gitlab.com/onekind/layout-vue/commit/0e5a6624dfbaeb6daccc485ca18563f3b629d7d2))

## [2.0.4](https://gitlab.com/onekind/layout-vue/compare/v2.0.3...v2.0.4) (2022-03-18)


### Bug Fixes

* **grow:** row grow now sets width 100% as well ([2f3a1ef](https://gitlab.com/onekind/layout-vue/commit/2f3a1eff2559d08bc92bfbd23f082c5a96fff2c5))

## [2.0.3](https://gitlab.com/onekind/layout-vue/compare/v2.0.2...v2.0.3) (2022-02-07)


### Bug Fixes

* wrong gap resolution implementation ([6f07cf4](https://gitlab.com/onekind/layout-vue/commit/6f07cf444e11a544ce629858a2d674d025bf8984))

## [2.0.2](https://gitlab.com/onekind/layout-vue/compare/v2.0.1...v2.0.2) (2022-02-06)


### Bug Fixes

* wrong export strategy ([8a3817e](https://gitlab.com/onekind/layout-vue/commit/8a3817e6ee005388ebd705245b03eeb98c109cf1))

## [2.0.1](https://gitlab.com/onekind/layout-vue/compare/v2.0.0...v2.0.1) (2022-02-06)


### Bug Fixes

* **guides:** join guides code and style into the layout package ([5b6cf38](https://gitlab.com/onekind/layout-vue/commit/5b6cf389403ee6f9510b96add1ee6b086c7d0d4f))

# [2.0.0](https://gitlab.com/onekind/layout-vue/compare/v1.0.2...v2.0.0) (2022-02-06)


### Code Refactoring

* **alignment:** make align and justify classes more aligned with flex rules. ([260c0ae](https://gitlab.com/onekind/layout-vue/commit/260c0ae6515e5042f65385c0c40f5dbc94d44219))


### BREAKING CHANGES

* **alignment:** new align and justify classes

## [1.0.2](https://gitlab.com/onekind/layout-vue/compare/v1.0.1...v1.0.2) (2022-02-06)


### Bug Fixes

* **watch:** syntax update frmo watch to watchEffect ([a7e6764](https://gitlab.com/onekind/layout-vue/commit/a7e6764dd8b9364429bdca73bbd7a921de8d85c7))

## [1.0.1](https://gitlab.com/onekind/layout-vue/compare/v1.0.0...v1.0.1) (2022-02-03)


### Bug Fixes

* split namespace and package name for build purposes ([d365384](https://gitlab.com/onekind/layout-vue/commit/d365384f3474be7c4cef108ecef4e835ffca6a20))

# 1.0.0 (2022-02-03)


### Bug Fixes

* wrong eslint version ([af6c53e](https://gitlab.com/onekind/layout-vue/commit/af6c53e3d46e14848e0a4dca5c1d1b300c16b185))


### Features

* initial commit ([a755fdd](https://gitlab.com/onekind/layout-vue/commit/a755fdd0e58e8e6d1da0730001a3cff291027fde))
