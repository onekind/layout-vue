# Wrap

- **Basic** `nowrap`

<div class="sample">
  <ok-layout is="ul" wrap="nowrap">
    <ok-layout is="li" dir="col">First</ok-layout>
    <ok-layout is="li" dir="col">Second</ok-layout>
    <ok-layout is="li" dir="col">Third</ok-layout>
    <ok-layout is="li" dir="col">Fourth</ok-layout>
    <ok-layout is="li" dir="col">Fifth</ok-layout>
    <ok-layout is="li" dir="col">Sixth</ok-layout>
    <ok-layout is="li" dir="col">Seventh</ok-layout>
    <ok-layout is="li" dir="col">Eighth</ok-layout>
    <ok-layout is="li" dir="col">Ninth</ok-layout>
    <ok-layout is="li" dir="col">Tenth</ok-layout>
  </ok-layout>
  <ok-layout is="ul" wrap="wrap">
    <ok-layout is="li" dir="col">First</ok-layout>
    <ok-layout is="li" dir="col">Second</ok-layout>
    <ok-layout is="li" dir="col">Third</ok-layout>
    <ok-layout is="li" dir="col">Fourth</ok-layout>
    <ok-layout is="li" dir="col">Fifth</ok-layout>
    <ok-layout is="li" dir="col">Sixth</ok-layout>
    <ok-layout is="li" dir="col">Seventh</ok-layout>
    <ok-layout is="li" dir="col">Eighth</ok-layout>
    <ok-layout is="li" dir="col">Ninth</ok-layout>
    <ok-layout is="li" dir="col">Tenth</ok-layout>
  </ok-layout>
  <ok-layout is="ul" wrap="reverse">
    <ok-layout is="li" dir="col">First</ok-layout>
    <ok-layout is="li" dir="col">Second</ok-layout>
    <ok-layout is="li" dir="col">Third</ok-layout>
    <ok-layout is="li" dir="col">Fourth</ok-layout>
    <ok-layout is="li" dir="col">Fifth</ok-layout>
    <ok-layout is="li" dir="col">Sixth</ok-layout>
    <ok-layout is="li" dir="col">Seventh</ok-layout>
    <ok-layout is="li" dir="col">Eighth</ok-layout>
    <ok-layout is="li" dir="col">Ninth</ok-layout>
    <ok-layout is="li" dir="col">Tenth</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1,13,25}
<ok-layout is="ul" wrap="nowrap">
  <ok-layout is="li" dir="col">First</ok-layout>
  <ok-layout is="li" dir="col">Second</ok-layout>
  <ok-layout is="li" dir="col">Third</ok-layout>
  <ok-layout is="li" dir="col">Fourth</ok-layout>
  <ok-layout is="li" dir="col">Fifth</ok-layout>
  <ok-layout is="li" dir="col">Sixth</ok-layout>
  <ok-layout is="li" dir="col">Seventh</ok-layout>
  <ok-layout is="li" dir="col">Eighth</ok-layout>
  <ok-layout is="li" dir="col">Ninth</ok-layout>
  <ok-layout is="li" dir="col">Tenth</ok-layout>
</ok-layout>
<ok-layout is="ul" wrap="wrap">
  <ok-layout is="li" dir="col">First</ok-layout>
  <ok-layout is="li" dir="col">Second</ok-layout>
  <ok-layout is="li" dir="col">Third</ok-layout>
  <ok-layout is="li" dir="col">Fourth</ok-layout>
  <ok-layout is="li" dir="col">Fifth</ok-layout>
  <ok-layout is="li" dir="col">Sixth</ok-layout>
  <ok-layout is="li" dir="col">Seventh</ok-layout>
  <ok-layout is="li" dir="col">Eighth</ok-layout>
  <ok-layout is="li" dir="col">Ninth</ok-layout>
  <ok-layout is="li" dir="col">Tenth</ok-layout>
</ok-layout>
<ok-layout is="ul" wrap="reverse">
  <ok-layout is="li" dir="col">First</ok-layout>
  <ok-layout is="li" dir="col">Second</ok-layout>
  <ok-layout is="li" dir="col">Third</ok-layout>
  <ok-layout is="li" dir="col">Fourth</ok-layout>
  <ok-layout is="li" dir="col">Fifth</ok-layout>
  <ok-layout is="li" dir="col">Sixth</ok-layout>
  <ok-layout is="li" dir="col">Seventh</ok-layout>
  <ok-layout is="li" dir="col">Eighth</ok-layout>
  <ok-layout is="li" dir="col">Ninth</ok-layout>
  <ok-layout is="li" dir="col">Tenth</ok-layout>
</ok-layout>
```
:::

- **Responsive** `:wrap="{phoneSmall: 'wrap', desktopSmall: 'reverse'}"`

<div class="sample">
  <ok-layout is="ul" :wrap="{phoneSmall: 'wrap', desktopSmall: 'reverse'}">
    <ok-layout is="li" dir="col">First</ok-layout>
    <ok-layout is="li" dir="col">Second</ok-layout>
    <ok-layout is="li" dir="col">Third</ok-layout>
    <ok-layout is="li" dir="col">Fourth</ok-layout>
    <ok-layout is="li" dir="col">Fifth</ok-layout>
    <ok-layout is="li" dir="col">Sixth</ok-layout>
    <ok-layout is="li" dir="col">Seventh</ok-layout>
    <ok-layout is="li" dir="col">Eighth</ok-layout>
    <ok-layout is="li" dir="col">Ninth</ok-layout>
    <ok-layout is="li" dir="col">Tenth</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1}
<ok-layout is="ul" :wrap="{phoneSmall: 'wrap', desktopSmall: 'reverse'}">
  <ok-layout is="li" dir="col">First</ok-layout>
  <ok-layout is="li" dir="col">Second</ok-layout>
  <ok-layout is="li" dir="col">Third</ok-layout>
  <ok-layout is="li" dir="col">Fourth</ok-layout>
  <ok-layout is="li" dir="col">Fifth</ok-layout>
  <ok-layout is="li" dir="col">Sixth</ok-layout>
  <ok-layout is="li" dir="col">Seventh</ok-layout>
  <ok-layout is="li" dir="col">Eighth</ok-layout>
  <ok-layout is="li" dir="col">Ninth</ok-layout>
  <ok-layout is="li" dir="col">Tenth</ok-layout>
</ok-layout>
```
:::
