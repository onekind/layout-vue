---
sidebarDepth: 2
---

# What is Layout-Vue?

It's a functional class based solution to help developers implement responsibe layouts in Vue3 applications, based on css flexbox api.

## Motivation

Developing frontend products often leads to many hours of layout programming. Modern web applications need to make the most use of every medium available. Being it mobile phones, tablets, desktop.

Layout Vue introduces standardization so that developers are able to focus more on the business logic, and still deliver cutting edge, maintainable interfaces, aligned with the inteded UI/UX.

The dynamic properties also enable easy integration with a CMS, so that content editors can choose the various mutations the layout may change over each viewport size.

## Technology

### It Uses Vue 3

Vue 3's [composition API](https://v3.vuejs.org/api/composition-api.html) enables better code structure when compared to previous Options API in VueJS.

### It Uses Dart SASS

Leveraging the power of [dart-sass](https://sass-lang.com/dart-sass) mixins, an extensive number of classes can be dynamically generated with no effort.

### It Uses Vite Under The Hood

[Vite](https://vitejs.dev/), the next generation frontend Tooling.

- Faster dev server start
- Faster hot updates
- Faster build (uses Rollup internally)

<img style="display: block; margin-left: auto; margin-right: auto; width: 128px;" src="/logo.svg" alt="logo" />
