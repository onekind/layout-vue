# Direction

- **Basic** `dir="row"` and `dir="col"`

<div class="sample">
  <ok-layout dir="row">
    <ok-layout dir="col">Side</ok-layout>
    <ok-layout dir="col">by</ok-layout>
    <ok-layout dir="col">Side</ok-layout>
  </ok-layout>
</div>

<div class="sample">
  <ok-layout dir="col">
    <ok-layout dir="row">Top</ok-layout>
    <ok-layout dir="row">Middle</ok-layout>
    <ok-layout dir="row">Bottom</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1,6}
<ok-layout dir="row">
  <ok-layout dir="col">Side</ok-layout>
  <ok-layout dir="col">by</ok-layout>
  <ok-layout dir="col">Side</ok-layout>
</ok-layout>
<ok-layout dir="col">
  <ok-layout dir="col">Top</ok-layout>
  <ok-layout dir="col">Middle</ok-layout>
  <ok-layout dir="col">Bottom</ok-layout>
</ok-layout>
```
:::

- **Responsive** `:dir="{phoneSmall: 'col', tabletSmall: 'row'"`

::: info
You need to modify the viewport `width` to see the result.
:::

<div class="sample">
  <ok-layout :dir="{phoneSmall: 'col', tabletSmall: 'row'}">
    <ok-layout :dir="{phoneSmall: 'row', tabletSmall: 'col'}">First</ok-layout>
    <ok-layout :dir="{phoneSmall: 'row', tabletSmall: 'col'}">Second</ok-layout>
    <ok-layout :dir="{phoneSmall: 'row', tabletSmall: 'col'}">Third</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1}
<ok-layout :dir="{phoneSmall: 'col', tabletSmall: 'row'}">
  <ok-layout :dir="{phoneSmall: 'row', tabletSmall: 'col'}">First</ok-layout>
  <ok-layout :dir="{phoneSmall: 'row', tabletSmall: 'col'}">Second</ok-layout>
  <ok-layout :dir="{phoneSmall: 'row', tabletSmall: 'col'}">Third</ok-layout>
</ok-layout>
```
:::

- **Mixed responsive** `:dir="{phoneSmall: 'row', tabletSmall: 'col'}"`

<div class="sample">
  <ok-layout :dir="{phoneSmall: 'row', tabletSmall: 'col'}">
    <ok-layout :dir="{phoneSmall: 'col', tabletSmall: 'row'}" grow>One</ok-layout>
    <ok-layout :dir="{phoneSmall: 'col', tabletSmall: 'row'}" grow>Two</ok-layout>
    <ok-layout :dir="{phoneSmall: 'col', tabletSmall: 'row'}" grow>Three</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1}
<ok-layout :dir="{phoneSmall: 'row', tabletSmall: 'col'}">
  <ok-layout :dir="{phoneSmall: 'col', tabletSmall: 'row'}" grow>One</ok-layout>
  <ok-layout :dir="{phoneSmall: 'col', tabletSmall: 'row'}" grow>Two</ok-layout>
  <ok-layout :dir="{phoneSmall: 'col', tabletSmall: 'row'}" grow>Three</ok-layout>
</ok-layout>
```
:::
