# @okekind/layout-vue

![logo](https://gitlab.com/onekind/layout-vue/raw/master/docs/public/logo.png)

[![build status](https://img.shields.io/gitlab/pipeline/onekind/layout-vue/master.svg?style=for-the-badge)](https://gitlab.com/onekind/layout-vue.git)
[![npm-publish](https://img.shields.io/npm/dm/@onekind/layout-vue.svg?style=for-the-badge)](https://www.npmjs.com/package/@onekind/layout-vue)
[![release](https://img.shields.io/npm/v/@onekind/layout-vue?label=%40onekind%2Flayout-vue%40latest&style=for-the-badge)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?style=for-the-badge)](https://github.com/semantic-release/semantic-release)

A functional class based solution to help developers implement responsibe layouts in Vue3 applications, based on css flexbox api.

Checkout the [Demo](https://onekind.gitlab.io/layout-vue/) which contains the component documentation.

> If you enjoy this component, feel free to drop me feedback, either via the repository, or via jose@onekind.io.

## Instalation

```bash
yarn add @onekind/layout-vue
```

## Setting up

- Add the following to you application main.js file:

```js
import {OkLayout} from '@onekind/layout-vue'
import '@onekind/layout-vue/dist/layout-vue.css'
app.component(OkLayout.name, OkLayout)
```

- **optionally** you may also import the Guide helper.

```js
import {OkGuides} from '@onekind/layout-vue'
import '@onekind/layout-vue/dist/guides-vue.css'
app.component(OkGuides.name, OkGuides)
```

## Customizing

You may use the `scss` files instead, which offer more customazation.

- **breakpoints**: Optionally setup your breakpoint names and values, before you import the main `scss` file.

```scss
$breakpoints: (
  "phone": 320,
  "tablet": 425,
  "desktop": 1024
);
```

- **width**: Optionally setup a new application max layout width, before you import the main `scss` file.

```scss
$layout-max-width: 1080;
```

- **gap**: Optionally setup new gap base value and gap rations, before you import the main `scss` file.

```scss
$layout-gap-base: 16;
```

```scss
@use "sass:math";
$gap-ratios: (
  "small": math.div($layout-gap-base, 2)px,
  "medium": #{$layout-gap-base}px,
  "large": #{$layout-gap-base * 2}px,
);
```

- **columns**: Optionally setup new column limit for the guides helper, before you import the main `scss` file.

```scss
$layout-columns: 1080;
```

- Import the main style `scss` file.

```scss
@import '@onekind/layout-vue/src/styles/layout.scss';
```
