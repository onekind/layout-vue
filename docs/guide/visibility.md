# Visibiliy

- **Basic** `:visible="false"`

<div class="sample">
  <ok-layout dir="row">
    <ok-layout dir="col" :visible="false">First</ok-layout>
    <ok-layout dir="col">Second</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{2}
<ok-layout dir="row">
  <ok-layout dir="col" :visible="false">First</ok-layout>
  <ok-layout dir="col">Second</ok-layout>
</ok-layout>
```
:::

- **Responsive** `:visible="{phoneSmall: false, desktopSmall: true}"`

::: info
You need to modify the viewport `width` to see the result.
:::

<div class="sample">
  <ok-layout dir="row">
    <ok-layout dir="col" :visible="{phoneSmall: false, desktopSmall: true}">First</ok-layout>
    <ok-layout dir="col">Second</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{2}
<ok-layout dir="row">
  <ok-layout dir="col" :visible="false">First</ok-layout>
  <ok-layout dir="col">Second</ok-layout>
</ok-layout>
```
:::
