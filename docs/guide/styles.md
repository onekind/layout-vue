# The styles

## Breakpoints

layout-vue ships with **predefined** `$breakpoints` variable which you can override, prior to importing the `main.scss` file:

| Name          | Value   |
| ------------- |:-------:|
| phone-small   | 320     |
| phone-large   | 375     |
| tablet-small  | 425     |
| tablet-large  | 768     |
| desktop-small | 1024    |
| desktop-large | 1440    |

The names are important, because you can use them to define rules **per breakpoint**.

![An image](/breakpoints.svg)

## Columns and Gap

Columns come **predefined** to `12`, but you may use any other number of columns that best suits your design.

The optional gap is set as an initial unit of `8`, and a **predefined** set of gap options is provided. These can also be defined for more or less granularity.

| Name        | Rap ratio | Result  |
| ----------- |:---------:| -------:|
| extra-small | 1/4       | 2px     |
| small       | 1/2       | 4px     |
| medium      | 1/1       | 8px     |
| large       | 1*2       | 16px    |
| extra-large | 1.4       | 32px    |
