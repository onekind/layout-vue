import DefaultTheme from 'vitepress/theme'
import '../../../src/styles/styleguide.scss'
import './theme.scss'

import '../../../src/styles/layout.scss'
import components from '../../../src/index.js'

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.use(components)
  },
}
