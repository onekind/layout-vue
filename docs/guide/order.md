# Order

- **Basic** `reverse`

<div class="sample">
  <ok-layout dir="row" reverse>
    <ok-layout dir="col">First</ok-layout>
    <ok-layout dir="col">Second</ok-layout>
    <ok-layout dir="col">Third</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1}
<ok-layout dir="row" reverse>
  <ok-layout dir="col">First</ok-layout>
  <ok-layout dir="col">Second</ok-layout>
  <ok-layout dir="col">Third</ok-layout>
</ok-layout>
```
:::

- **Responsive** `:reverse="['desktopSmall']"`

::: info
You need to modify the viewport `width` to see the result.
:::

<div class="sample">
  <ok-layout :dir="{desktopSmall: 'col'}" :reverse="['desktopSmall']" >
    <ok-layout>First</ok-layout>
    <ok-layout>Second</ok-layout>
    <ok-layout>Third</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{1}
<ok-layout :dir="{desktopSmall: 'col'}" :reverse="['desktopSmall']" >
  <ok-layout>First</ok-layout>
  <ok-layout>Second</ok-layout>
  <ok-layout>Third</ok-layout>
</ok-layout>
```
:::

- **Basic** `first`

<div class="sample">
  <ok-layout dir="row">
    <ok-layout dir="col">First</ok-layout>
    <ok-layout dir="col">Second</ok-layout>
    <ok-layout dir="col" first>Third!!!</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{4}
<ok-layout dir="row">
  <ok-layout dir="col">First</ok-layout>
  <ok-layout dir="col">Second</ok-layout>
  <ok-layout dir="col" first>Third!!!</ok-layout>
</ok-layout>
```
:::

- **Basic** `last`

<div class="sample">
  <ok-layout dir="row">
    <ok-layout dir="col">First</ok-layout>
    <ok-layout dir="col" last>Second!!!</ok-layout>
    <ok-layout dir="col">Third</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{3}
<ok-layout dir="row">
  <ok-layout dir="col">First</ok-layout>
  <ok-layout dir="col" last>Second!!!</ok-layout>
  <ok-layout dir="col">Third</ok-layout>
</ok-layout>
```
:::

- **Responsive** `:first="['tabletSmall']" :last="['desktopSmall']"`

<div class="sample">
  <ok-layout dir="row">
    <ok-layout dir="col">First</ok-layout>
    <ok-layout :dir="{tabletSmall: 'col', desktopSmall: 'col'}" :first="['tabletSmall']" :last="['desktopSmall']">Second!!!</ok-layout>
    <ok-layout dir="col">Third</ok-layout>
  </ok-layout>
</div>

::: details Code
```vue{3}
<ok-layout dir="row">
  <ok-layout dir="col">First</ok-layout>
    <ok-layout dir="col" :first="['tabletSmall']" :last="['desktopSmall']">Second!!!</ok-layout>
  <ok-layout dir="col">Third</ok-layout>
</ok-layout>
```
:::
